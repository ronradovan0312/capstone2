const Product = require('../models/Product.js');


// Create Product
module.exports.addProduct = (request, response) => {
    let new_product = new Product({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price
    });

    // console.log("New Product:", new_product); // Log the new product object before saving	

   return new_product.save().then((saved_product, error) => {
			if(error) {
				return response.status(500).send(error.message);
			}

            return response.send("Product added successfully");
        }).catch(error => response.send(error));
}

// Retrieve all products
module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}


// Retrieve all active products
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}


// Retrieve single product
module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}

// Update product information
module.exports.updateProduct = (request, response) => {
	let updated_product_details = {
		name: request.body.name,
		description: request.body.description,
		isActive: request.body.isActive,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((product, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Product has been updated successfully!'
		})
	})
}


// Archive product
module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, { isActive: false }).then((product, error) => {
		if(error){
			return response.send(false);
		}

		return response.send("Products successfully archived");
	})
}


// Activate product
module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.id, { isActive: true }).then((product, error) => {
		if(error){
			return response.send(false);
		}

		return response.send("Products successfully activated");
	})
}

  