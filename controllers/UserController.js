const User = require('../models/User.js');
const Product = require('../models/Product.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');
let salt = bcrypt.genSaltSync(10);


// User registration
module.exports.registerUser = (request_body) => {
	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save()
        .then((registered_user) => {
            return {
                message: 'Successfully registered a user!'
            };
        })
        .catch(error => {
            return {
                message: error.message
            };
        });
}


// User login
module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		// Checks if a user is found with an existing email
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			}) 
		}

		// If a user was found with an existing email, then check if the password of that user matched the input from the request body
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			// If the password comparison returns true, then respond with the newly generated JWT access token.
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

// get user profile
module.exports.getProfile = (request_body) => {

	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}
		user.password = "";

		return user;
	}).catch(error => console.log(error));

};

// checkout products
module.exports.checkout = async (request, response) => {
	// validates if user is admin. If user is admin, return "Action is Forbidden for admin users"
	if(request.user.isAdmin == true){
		return response.send('Action is Forbidden for admin users');
	}

	// [SECTION] Updating User Collection
	// This variable will return true once the user data has been updated
	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let new_order = {
			orderId: request.body.orderId
		}

		user.orders.push(new_order);

		return user.save().then(updated_user => true).catch(error => error.message);
	})

	// Sends any error within 'isUserUpdated' as a response
	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}

	// [SECTION] Updating product collection
	let isProductUpdated = await Product.findById(request.body.orderId).then(product => {
		console.log(request.user.id);
		let new_user = {
			userId: request.user.id
		}

		product.orders.push(new_user);

		return product.save().then(updated_product => true).catch(error => error.message)
	})


	if(isProductUpdated !== true){
		return response.send({ message: isProductUpdated });
	}

	// [SECTION] Once isUserUpdated AND isProductUpdated return true
	if(isUserUpdated && isProductUpdated){
		return response.send({ message: 'Order successfully placed' });
	}
}


