const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// User Registration
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
});

// User Login
router.post('/login', (request, response) => {
	UserController.loginUser(request, response)
});

// Non-admin User checkout
router.post('/checkout', auth.verify, (request, response) => {
	UserController.checkout(request, response)
});

// Retrieve User Details

router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/checkout', auth.verify, (request, response) => {
	UserController.checkout(request, response);
})


module.exports = router;