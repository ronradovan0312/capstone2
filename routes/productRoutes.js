const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

// Create single Product
router.post('/addProduct', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response)
});

// Retrieve all products
router.get('/allProducts', (request, response) => {
	ProductController.getAllProducts(request, response);
});

// Retrieve all active products
router.get('/activeProducts', (request, response) => {
	ProductController.getAllActiveProducts(request, response)
});

// Retrieve single product
router.get('/getProductById/:id', (request, response) => {
	ProductController.getProduct(request, response)
});

// Update Product information
router.put('/updateProduct/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response)
});

// Archive Product
router.put('/archiveProduct/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response)
});

// Activate Product
router.put('/activateProduct/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response)
});

module.exports = router;