const mongoose = require('mongoose');

// Schema
const user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'E-mail is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, 'Course ID is required!']
			},
			orderDate: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Ordered'
			}
		}
	]
	
})

module.exports = mongoose.model('User', user_schema);