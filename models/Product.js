const mongoose = require('mongoose');

// Schema
const product_schema = new mongoose.Schema({
    name : {
        type : String,
        // Requires the data for this our fields/properties to be included when creating a record.
        // The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
        required : [true, "Product is required"]
    },
    description : {
        type : String,
        required : [true, "Description is required"]
    },
    price : {
        type : Number,
        required : [true, "Price is required"]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        // The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
        default : new Date()
    },
    orders : [
        {
            userId : {
                type : String,
                required: [true, "UserId is required"]
            },
            orderDate : {
                type : Date,
                default : new Date() 
            }
        }
    ]

})

module.exports = mongoose.model("Product", product_schema);